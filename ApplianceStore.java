import java.util.Scanner;
public class ApplianceStore {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Microwave[] microwaves = new Microwave[4];
		for(int i = 0; i < microwaves.length; i++) {
			microwaves[i] = new Microwave();
			System.out.println("What is the brand your looking for?");
			microwaves[i].brand = reader.nextLine();
			System.out.println("What is the color your looking for?");
			microwaves[i].color = reader.nextLine();
			System.out.println("What is the average price your looking for?");
			microwaves[i].price = Double.parseDouble(reader.nextLine());
		}
		
		System.out.println(microwaves[3].brand);
		System.out.println(microwaves[3].color);
		System.out.println(microwaves[3].price);
		
		microwaves[0].displayBrand();
		microwaves[0].reheatFood();
	}
	
	
	
}